export const infoCards = [
    {
        title: 'Make payment seamless',
        description: 'Online or in-store, Quadpay means more flexibility at checkout.',
        icon: '/images/smartphone.png',
    },
    {
        title: 'Take on zero risk',
        description: 'You get paid today. Your customers pay over time.',
        icon: '/images/shield-check.png',
    },
    {
        title: 'Put your customers first',
        description: 'Interest-free installment plans with no hard credit check or impact on credit.',
        icon: '/images/people-cart.png',
    },
    {
        title: 'Accept all major cards',
        description: 'QuadPay is the only Buy Now, Pay Later solution that accepts Amex and Discover along with Visa.',
        icon: '/images/credit-card-check.png',
    },
    {
        title: 'Tap into our community',
        description: 'Introduce your brand to millions of existing QuadPay customers in our marketplace.',
        icon: '/images/fireworks-people.png',
    },
    {
        title: 'Track progress',
        description: 'Easily manage customer data, order management, and reconciliation in the merchant portal.',
        icon: '/images/graph-bar.png',
    },
];
