import React, { FC } from "react";

import './InfoSection.scss';
import { infoCards } from 'fixtures';
import { InfoCard, InfoCardProps } from 'components/InfoCard';

type InfoSectionProps = {
    title: string;
}

export const InfoSection: FC<InfoSectionProps> = ({ title }) => (
    <div className="InfoSection">
        <span className="sectionTitle">{title}</span>
        <div className="cardsList">
            {infoCards.map(({title, description, icon}: InfoCardProps) => (
                <InfoCard
                    key={title}
                    title={title}
                    description={description}
                    icon={icon}
                />
            ))}
        </div>
    </div>
);
