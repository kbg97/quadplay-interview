import React, { FC } from "react";

import './InfoCard.scss';

export type InfoCardProps = {
    title: string;
    description: string;
    icon: string;
}

export const InfoCard: FC<InfoCardProps> = ({ title, description, icon }) => (
    <div className="InfoCard">
        <img src={icon} alt="CardIcon" className="cardIcon"/>
        <span className="cardTitle">{title}</span>
        <span className="cardBody">{description}</span>
    </div>
);
