import React, { FC } from "react";

import './Banner.scss';
import {StoreButton} from 'components/StoreButton';
import bannerQ from 'assets/banner-q.png';
import appleIcon from 'assets/apple.png';
import playStore from 'assets/play-store.png';

export const Banner: FC = () => (
    <div className="Banner">
        <img src={bannerQ} className="bannerQ" alt="QBannerPiece"/>
        <div className="leftBannerContainer">
            <span className="text title bold">
                 Quadpay anywhere
            </span>
            <span className="text description">
                 Shop your favorite brands with the Quadpay app—online or in-store.
            </span>
            <div className="buttonsContainer">
                <StoreButton
                    text="Get on the App Store"
                    url="https://apple.com"
                    icon={appleIcon}
                />
                <StoreButton
                    text="Get on Google Play"
                    url="https://google.com"
                    icon={playStore}
                />
            </div>
        </div>
    </div>
);
