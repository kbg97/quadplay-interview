import React, { FC } from "react";

import './StoreButton.scss';

type StoreButtonProps = {
    text: string;
    url: string;
    icon?: string;
}

export const StoreButton: FC<StoreButtonProps> = ({text, url, icon}) => (
    <div className="StoreButton" onClick={() => window.location.href = url}>
        {
            icon ?
                (
                    <img src={icon} alt="Icon" className="buttonIcon" />
                )
            :
                null
        }
        <span className="buttonText">{text}</span>
    </div>
);
