import React, {FC} from "react";

import './Home.scss';
import logo from "assets/logo.png";
import { Banner } from "components/Banner";
import { InfoSection } from 'components/InfoSection';

export const Home: FC = () => (
    <div className="Home">
        <img src={logo} className="FullScreenLogo" alt="Logo" />
        <div className="pageContent">
            <Banner />
            <InfoSection title="Partnering with Quadpay" />
        </div>
    </div>
);
